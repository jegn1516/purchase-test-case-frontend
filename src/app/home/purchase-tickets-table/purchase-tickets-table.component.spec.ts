import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseTicketsTableComponent } from './purchase-tickets-table.component';

describe('PurchaseTicketsTableComponent', () => {
  let component: PurchaseTicketsTableComponent;
  let fixture: ComponentFixture<PurchaseTicketsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseTicketsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseTicketsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
