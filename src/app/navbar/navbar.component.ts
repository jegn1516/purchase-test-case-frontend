import { Component, OnInit } from '@angular/core';
import {SessionService} from '../services/session.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from '../entities/User';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user$: Observable<User>;
  isMenuCollapsed = true;

  constructor(private sessionService: SessionService, private router: Router) { }

  ngOnInit() {
    this.user$ = this.sessionService.getUser();
  }

  logout() {
    this.sessionService.logout().subscribe( isLogout => {
      if (isLogout) {
        this.router.navigate(['/login']);
      }
    });
  }

}
