import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {User} from '../entities/User';
import {OauthToken} from '../entities/OauthToken';
import {AuthenticationService} from './authentication.service';
import {log} from 'util';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private user: Observable<User>;
  private token: OauthToken;

  constructor(private authenticationService: AuthenticationService) {
    const tokenString = localStorage.getItem('token');
    if (tokenString != null) {
      const token = JSON.parse(tokenString);
      this.token = token;
      this.getUser();
    }
  }

  login(user: User, persistent: boolean): Observable<boolean> {
    return this.authenticationService.login(user).pipe(
      map(oauthToken => {
        // Store token on session
        this.token = oauthToken;
        if ( persistent ) {
          log(JSON.stringify(oauthToken));
          localStorage.setItem('token', JSON.stringify(oauthToken));
        }
        return true;
      })
    );
  }

  register(user: User ): Observable<boolean> {
    return this.authenticationService.register(user).pipe(
      map(oauthToken => {
        // Store token on session
        this.token = oauthToken;
        log(JSON.stringify(oauthToken));
        localStorage.setItem('token', JSON.stringify(oauthToken));

        return true;
      })
    );
  }

  logout(): Observable<boolean> {
    return this.authenticationService.logout(this.token).pipe(
      map( response => {
        if (localStorage.getItem('token')) {
          localStorage.removeItem('token');
        }
        this.user = undefined;
        this.token = undefined;
        return true;
      })
    );
  }

  isLogin(): Observable<boolean> {

    return this.getUser().pipe(
      map(user => {
          return user != null;
        },
        catchError(error => {
          return of(false);
        }))
    );
  }

  getToken(): OauthToken {
    return this.token;
  }

  getUser(): Observable<User> {
    if ( this.token === undefined) {
      return of(undefined);
    }
    if (this.user === undefined) {
      this.user = this.authenticationService.getUser(this.token).pipe(
        catchError( error => {
          this.user = undefined;
          return of(undefined);
        })
      );
    }
    return this.user;
  }
}
