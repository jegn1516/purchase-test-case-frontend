import {BaseEntity} from './BaseEntity';

export class PaginationRequest extends BaseEntity {
  size: number;
  page: number;
  filter: string;
}
