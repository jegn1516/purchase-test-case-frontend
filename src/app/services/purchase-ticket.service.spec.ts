import { TestBed } from '@angular/core/testing';

import { PurchaseTicketService } from './purchase-ticket.service';

describe('PurchaseTicketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PurchaseTicketService = TestBed.get(PurchaseTicketService);
    expect(service).toBeTruthy();
  });
});
