import {BaseEntity} from './BaseEntity';

export class User extends BaseEntity {
  username: string;
  password: string;
  passwordConfirmation: string;
  name: string;
  lastname: string;
  phoneNumber: string;
  birthday: string;
  termsAndConditions: boolean;
  privacyNotice: boolean;
}
