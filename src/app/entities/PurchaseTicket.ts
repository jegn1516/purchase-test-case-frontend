import {BaseEntity} from './BaseEntity';

export class PurchaseTicket extends BaseEntity {
  store: string;
  totalAmount: number | string;
  voucher: File | string;
}
