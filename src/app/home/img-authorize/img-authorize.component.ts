import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {flatMap, map} from 'rxjs/operators';
import {DomSanitizer} from '@angular/platform-browser';
import {SessionService} from '../../services/session.service';
import {environment} from '../../../environments/environment';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'img-authorize',
  template: `<img [src]="dataUrl$|async" [class]="class" [alt]="alt"/>`,
})
export class ImgAuthorizeComponent implements OnChanges {
  @Input() private src: string;
  @Input() class: string;
  @Input() alt: string;
  private src$ = new BehaviorSubject(this.src);
  dataUrl$ = this.src$.pipe(
    flatMap(url => this.loadImage(url))
  );
  fullUrl = `${environment.apiBaseUrl}/api/purchase_tickets/`;
  headers = {
    headers: new HttpHeaders({
      Authorization: `Bearer ${this.sessionService.getToken().accessToken}`,
    })
  };

  constructor(private http: HttpClient,
              private sessionService: SessionService,
              private domSanitizer: DomSanitizer) { }

  ngOnChanges(): void {
    this.src$.next(this.src);
  }

  private loadImage(url: string): Observable<any> {
    const fullUrl = `${this.fullUrl}${url}/voucher`;
    const headers = this.headers;
    headers['responseType'] = 'arrayBuffer';
    headers['observe'] = 'response';
    // @ts-ignore
    return this.http.get(
      fullUrl,
      headers
      ).pipe(
      map((response: any) => {
        const mimeType = response.headers.get('content-type');

        return this.domSanitizer.bypassSecurityTrustUrl(
          URL.createObjectURL(new Blob([response.body], { type: mimeType}))
        );
      })
    );
  }
}
