import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OauthToken} from '../entities/OauthToken';
import {User} from '../entities/User';
import {map, shareReplay} from 'rxjs/operators';
import {keysToCamel} from '../utilities/keysToCamel';
import {keysToSnake} from '../utilities/keysToSnake';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private baseUrl = `${environment.apiBaseUrl}/api/oauth`;

  constructor(private http: HttpClient) { }

  login(user: User): Observable<OauthToken> {
    const oauthRequest = {
      grantType: 'password',
      clientId: environment.clientId,
      clientSecret: environment.clientSecret,
      username: user.username,
      password: user.password,
      scope: '*'
    };

    return this.http.post<OauthToken>(`${this.baseUrl}/token`, keysToSnake(oauthRequest), httpOptions).pipe(
      map(oauthToken => {
        return Object.assign(new OauthToken(), keysToCamel(oauthToken));
      })
    );
  }

  logout(token: OauthToken): Observable<object> {
    const logoutOptions = {headers: httpOptions.headers.set('Authorization', `Bearer ${token.accessToken}`)};

    return this.http.delete<object>(`${this.baseUrl}/logout`, logoutOptions);
  }

  getUser(token: OauthToken): Observable<User> {

    const logoutOptions = {headers: httpOptions.headers.set('Authorization', `Bearer ${token.accessToken}`)};

    return this.http.get<User>(`${this.baseUrl}/profile`, logoutOptions).pipe(
      shareReplay(1),
      map(user => {
        return Object.assign(new User(), keysToCamel(user));
      })
    );
  }

  register(user: User): Observable<OauthToken> {

    return this.http.post<OauthToken>(`${this.baseUrl}/register`, keysToSnake(user)).pipe(
      map(oauthToken => {
        return Object.assign(new OauthToken(), keysToCamel(oauthToken));
      })
    );

  }
}
