import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseTicketsTableComponent } from './purchase-tickets-table/purchase-tickets-table.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HomeRoutingModule} from './home-routing.module';
import {FormsModule} from '@angular/forms';
import {NavbarModule} from '../navbar/navbar.module';
import { PurchaseTicketFormComponent } from './purchase-ticket-form/purchase-ticket-form.component';
import { ImgAuthorizeComponent } from './img-authorize/img-authorize.component';
import {TextMaskModule} from 'angular2-text-mask';

@NgModule({
  declarations: [PurchaseTicketsTableComponent, PurchaseTicketFormComponent, ImgAuthorizeComponent],
  imports: [
    CommonModule,
    NgbModule,
    HomeRoutingModule,
    FormsModule,
    NavbarModule,
    TextMaskModule
  ],
  entryComponents: [
    PurchaseTicketFormComponent
  ]
})
export class HomeModule { }
