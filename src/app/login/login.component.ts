import { Component, OnInit } from '@angular/core';
import {SessionService} from '../services/session.service';
import {Router} from '@angular/router';
import {log} from 'util';
import {User} from '../entities/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = new User();
  errorMessage;
  loading = false;
  isPersisted = false;

  constructor(private sessionService: SessionService, private router: Router) { }

  ngOnInit() {
    this.sessionService.isLogin().subscribe(isLogin => {
      if (isLogin) {
        this.sessionService.getUser().subscribe(user => {
          this.router.navigate(['home']);
        });
      }
    });
  }

  login(): void {

    this.loading = true;
    this.sessionService.login(this.user, this.isPersisted).subscribe(isLogin => {
      log('Server Response');
      this.errorMessage = undefined;
      this.loading = false;
      this.sessionService.getUser().subscribe(user => {
        this.router.navigate(['home']);
      });
    }, error => {
      log(error);
      if ( error.status === 400 ) {
        this.errorMessage = 'El nombre de usuario o la contraseña es incorrecta.';
      }
      this.loading = false;
    });
  }

}
