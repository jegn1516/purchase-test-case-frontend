import {toSnake} from './toSnake';

export function toFormData(o: object): FormData {
  const formData = new FormData();
  Object.keys(o).forEach(key => {
    formData.append(`${toSnake(key)}`, o[key]);
  });
  return formData;
}
