import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SessionService} from './session.service';
import {environment} from '../../environments/environment';
import {PurchaseTicket} from '../entities/PurchaseTicket';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {keysToCamel} from '../utilities/keysToCamel';
import {PaginationRequest} from '../entities/PaginationRequest';
import {toQueryParam} from '../utilities/toQueryParam';
import {toFormData} from '../utilities/toFormData';
import {Pagination} from '../entities/Pagination';

@Injectable({
  providedIn: 'root'
})
export class PurchaseTicketService {
  private baseUrl = `${environment.apiBaseUrl}/api/purchase_tickets`;

  constructor(private httpService: HttpClient, private sessionService: SessionService) { }

  all(paginationRequest: PaginationRequest): Observable<Pagination<PurchaseTicket>> {
    return this.httpService.get(`${this.baseUrl}?${toQueryParam(paginationRequest)}`,
      this.generateHeader()).pipe(
        map( (paginationObject: any) => {
          const list = [];
          paginationObject.elements.forEach( purchaseTicketJson => {
            list.push(this.generateEntity(purchaseTicketJson));
          });
          paginationObject.elements = list;
          return Object.assign(new Pagination<PurchaseTicket>(), keysToCamel(paginationObject));
        }
      )
    );
  }

  create(purchaseTicket: PurchaseTicket): Observable<PurchaseTicket> {
    const headers = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${this.sessionService.getToken().accessToken}`,
      })
    };
    return this.httpService.post(`${this.baseUrl}`, toFormData(purchaseTicket), headers).pipe(
      map( purchaseTicketJson => {
        return this.generateEntity(purchaseTicketJson);
      })
    );
  }

  get(id: number): Observable<PurchaseTicket> {
    return this.httpService.get(`${this.baseUrl}/${id}`, this.generateHeader()).pipe(
      map( purchaseTicketJson => {
        return this.generateEntity(purchaseTicketJson);
      })
    );
  }

  private generateEntity(data: object): PurchaseTicket {
    return Object.assign(new PurchaseTicket(), keysToCamel(data));
  }

  private generateHeader() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.sessionService.getToken().accessToken}`,
      })
    };
  }
}
