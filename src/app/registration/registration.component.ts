import { Component, OnInit } from '@angular/core';
import {User} from '../entities/User';
import {SessionService} from '../services/session.service';
import {Router} from '@angular/router';
import {log} from 'util';
import * as moment from 'moment';
import {keysToCamel} from '../utilities/keysToCamel';
import {copyObj} from '@angular/animations/browser/src/util';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  user = new User();
  loading = false;
  phoneMask = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  birthdayMask = [/[0-3]/, /\d/, '-', /[0-1]/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  usernameTaken = false;
  phoneTaken = false;
  birthdayInvalid = false;

  constructor(private sessionService: SessionService, private router: Router) { }

  ngOnInit() {
    this.sessionService.isLogin().subscribe(isLogin => {
      if (isLogin) {
        this.sessionService.getUser().subscribe(user => {
          this.router.navigate(['home']);
        });
      }
    });
  }

  register() {
    this.loading = true;
    const user = Object.assign(new User(), this.user);
    user.phoneNumber = user.phoneNumber.replace(/-/g, '');
    user.birthday = moment(user.birthday, 'DD-MM-YYYY').format('YYYY-MM-DD');
    this.sessionService.register(user).subscribe(isRegister => {
      log('Server Response');
      this.loading = false;
      this.sessionService.getUser().subscribe( _ => {
        this.router.navigate(['home']);
      });
    }, error => {
      log(error);
      if ( error.status === 400 ) {

        const errors = keysToCamel(error.error);
        if (errors.username !== undefined && errors.username.length > 0) {
          this.usernameTaken = true;
        }

        if (errors.phoneNumber !== undefined && errors.phoneNumber.length > 0) {
          this.phoneTaken = true;
        }
        log('Fields invalid');
      }
      this.loading = false;
    });
  }

  validateBirthday() {
    const birthday = moment(this.user.birthday, 'DD-MM-YYYY');
    this.birthdayInvalid = !birthday.isValid();
  }

}
